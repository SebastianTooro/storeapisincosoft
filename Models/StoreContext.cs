using Microsoft.EntityFrameworkCore;

namespace StoreApi.Models
{
    public class StoreContext : DbContext
    {
        public StoreContext(DbContextOptions<StoreContext> options) :base (options)
        {

        }

        public DbSet<Product> ProductItems {get; set;}
        
    }
}