using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using StoreApi.Models;



namespace StoreApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly StoreContext _context;

        public ProductsController(StoreContext context) => _context = context;

        //Get: api/products
        [HttpGet]
        public ActionResult<IEnumerable<Product>> GetProducts()
        {
            return _context.ProductItems;
        }

        //Get: api/products/{id}
        [HttpGet("{id}")]
        public ActionResult<Product> GetProductItem(int id)
        {
            var productItem = _context.ProductItems.Find(id);

            if(productItem == null)
            {
                return NotFound();
            }

            return productItem;
        }

        //Post: api/products
        [HttpPost]
        public ActionResult<Product> PostProductItem(Product product)
        {
            _context.ProductItems.Add(product);
            _context.SaveChanges();

            return CreatedAtAction("GetProductItem", new Product{id = product.id}, product);
        }

        //Put: api/products/{id}
        [HttpPut("{id}")]
        public ActionResult PutProductItem(int id, Product product)
        {
            if(id != product.id)
            {
                return BadRequest();
            }

            _context.Entry(product).State = EntityState.Modified;
            _context.SaveChanges();

            return NoContent();
        }

        //Delete api/product/{id}
        [HttpDelete("{id}")]
        public ActionResult<Product> DeleteProductItem(int id)
        {
            var productItem = _context.ProductItems.Find(id);
            if (productItem == null)
            {
                return NotFound();
            }

            _context.ProductItems.Remove(productItem);
            _context.SaveChanges();

            return productItem;
        }


    }
}